package com.classpath.inventory.model;

import java.time.LocalDate;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Order {
	
	private Long id;
	
	private LocalDate orderDate;
	
	private double price;
	
}
