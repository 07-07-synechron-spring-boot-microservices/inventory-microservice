package com.classpath.inventory.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/inventory")
public class InventoyRestController {

	private static int counter = 1000;
	
	@PostMapping
	public Integer updateOrder() {
		return --counter;
	}
	
	@GetMapping
	public Integer counter() {
		return counter;
	}
}
