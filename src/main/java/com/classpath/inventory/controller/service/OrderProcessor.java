package com.classpath.inventory.controller.service;

import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.stereotype.Service;

import com.classpath.orders.event.OrderEvent;

@Service
public class OrderProcessor {
	
	@StreamListener(Sink.INPUT)
	public void processOrder(OrderEvent orderEvent) {
		System.out.println("Processing the order Event :: "+ orderEvent);
		
	}
}
