package com.classpath.orders.event;

import java.time.LocalDate;

import com.classpath.inventory.model.Order;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;


@Builder
@AllArgsConstructor
@Data
@NoArgsConstructor
public class OrderEvent {
	
	private Order order;
	private LocalDate orderTime;
	private OrderStatus status;

}
